
# Note that the final version of this project was rebranded to *SSI Kit by walt.id* and is hosted at: https://github.com/walt-id/waltid-ssikit


LetsTrust.org project summary
===================

## The Project

LetsTrust.org offers Enterprises (B2B) and Consumers (B2C) Wallets under a permissive open source license (Apache 2), compliant with global and European Self-Sovereign Identity (SSI) standards.

SSI must become a commodity that is accessible to everyone. Therefore, our open source libraries and SDKs will enable developers / organizations to seamlessly adopt Europe’s emerging decentralized identity system - based on the EU Blockchain (EBSI) and the EU SSI Framework (ESSIF).

## The Company
LetsTrust is a European company building open source software in the field of Self-Sovereign Identity (SSI).

## The Mission
We enable developers to build identity and trust into the web and every application.

## Contact
Get in touch: office@letstrust.org  

## Funded & supported by

<img src="https://jolocom.io/wp-content/uploads/2020/10/eSSIF-lab-SSI-framework-logos-Jolocom-768x228.png">
